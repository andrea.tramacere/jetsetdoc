JetSpecComponent
================

.. currentmodule:: jetset.jet_model

.. autoclass:: JetSpecComponent
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~JetSpecComponent.plot

   .. rubric:: Methods Documentation

   .. automethod:: plot
