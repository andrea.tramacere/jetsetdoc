ModelMinimizer
==============

.. currentmodule:: jetset.minimizer

.. autoclass:: ModelMinimizer
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~ModelMinimizer.fit
      ~ModelMinimizer.get_fit_results

   .. rubric:: Methods Documentation

   .. automethod:: fit
   .. automethod:: get_fit_results
