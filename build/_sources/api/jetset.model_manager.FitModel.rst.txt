FitModel
========

.. currentmodule:: jetset.model_manager

.. autoclass:: FitModel
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~FitModel.PlotModel
      ~FitModel.add_component
      ~FitModel.eval
      ~FitModel.fit
      ~FitModel.get
      ~FitModel.get_conf_range
      ~FitModel.get_val
      ~FitModel.set
      ~FitModel.set_nu_grid
      ~FitModel.show_pars

   .. rubric:: Methods Documentation

   .. automethod:: PlotModel
   .. automethod:: add_component
   .. automethod:: eval
   .. automethod:: fit
   .. automethod:: get
   .. automethod:: get_conf_range
   .. automethod:: get_val
   .. automethod:: set
   .. automethod:: set_nu_grid
   .. automethod:: show_pars
